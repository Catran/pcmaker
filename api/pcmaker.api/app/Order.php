<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'fullname', 'phone'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}

<?php

namespace App\Http\Resources\Web;
use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'description' => $this->description,
            'price' => $this->price,
            'thumb' => $this->thumb,
            'brand' => [
                'id' => $this->brand->id,
                'title' => $this->brand->title,
                'slug' => $this->brand->slyg,
            ]
        ];
    }
}

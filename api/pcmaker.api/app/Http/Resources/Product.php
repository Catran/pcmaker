<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Brand as BrandResource;
use App\Http\Resources\Type as TypeResource;
class Product extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'description' => $this->description,
            'price' => $this->price,
            'brand' => new BrandResource($this->brand),
            'type' => new TypeResource($this->type),
            'thumb' => $this->thumb
        ];
    }
}

<?php

namespace App\Http\Controllers;
use App\Type;
use App\Http\Resources\Web\Type as Resource;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function types()
    {
        return response()->json(Resource::collection(Type::all()), 200);
    }
}

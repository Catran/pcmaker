<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Product;
use App\Http\Resources\Product as Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Resource::collection(Product::all()), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $folder = 'images/products_thumb/';
        if ($request->hasFile('thumb')) {
            $thumb = $request->file('thumb');
            $name = time().'.'.$thumb->getClientOriginalExtension();
            $data['thumb'] = $folder.$name;
            $file = $thumb->storeAs($folder, $name, 'public');
            $product = Product::create($data);
            return response()->json(new Resource($product), 201);
        } else {
            return response()->json(400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return response()->json(new Resource($product), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $data = $request->all();
        $folder = 'images/products_thumb/';
        if ($request->hasFile('thumb')) {
            Storage::disk('public')->delete('images/products_thumb/' . basename($product->thumb));
            $thumb = $request->file('thumb');
            $name = time().'.'.$thumb->getClientOriginalExtension();
            $data['thumb'] = $folder.$name;
            $file = $thumb->storeAs($folder, $name, 'public');
            $product->update($data);
            return response()->json(new Resource($product), 201);
        } else {
            $data['thumb'] = $product->thumb;
            $product->update($data);
            return response()->json(new Resource($product), 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        Storage::disk('public')->delete('images/products_thumb/' . basename($product->thumb));
        $product->delete();
        return response()->json(204);
    }
}

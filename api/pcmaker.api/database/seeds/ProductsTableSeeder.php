<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            /* moterboard */
            'MSI MPG Z390 GAMING EDGE AC (LGA1151v2, ATX)',
            'ESONIC B250-BTC-Gladiator (LGA1151, ATX)',
            'GIGABYTE GA-G41M-COMBO (LGA775, microATX)',
            'COLORFUL FM2/FM2+, mATX (C.A68M-BTC YV14)',
            'GIGABYTE GA-B250-FinTech (LGA1151, ATX)',
            'GIGABYTE GA-E2500N (Mini-ITX)',
            'MSI H310M PRO-VD PLUS, LGA1151v2, mATX (H310M PRO-VD PLUS)',
            'ASUS PRIME H310M-R R2.0 (LGA1151v2, mATX)',
            'ASRock H310CM-HDV (LGA1151v2, mATX)',
            'ASUS H110M-R/C/SI (LGA1151, microATX)',

            /* cpu */
            'INTEL Celeron G3930 LGA1151 OEM',
            'AMD Ryzen 3 1200 AM4 OEM',
            'INTEL Core i3-9100F LGA1151 OEM',
            'INTEL Core i3-9100F LGA1151 BOX',
            'AMD Ryzen 5 1600',
            'AMD Ryzen 3 3200G AM4 BOX',
            'AMD Ryzen 5 1600 AM4 BOX',
            'INTEL Core i3-9100 LGA1151 OEM',
            'AMD Ryzen 5 2600 AM4 OEM',
            'INTEL Core i5-9400F LGA1151 OEM',

            /* videocard */
            'ASUS Radeon RX 470 1206Mhz PCI-E 3.0 4096Mb 7000Mhz 256 bit 1xDVI-D Mining edition ОЕМ (MINING-RX470-4G-LED)',
            'INNO3D GeForce 1060 PCI-E 3.0 6144Mb (Samsung) 192 bit Mining Edition OEM (MN106F-5SDN-N5G)',
            'PowerColor Radeon RX 580 1215Mhz PCI-E 3.0 4096Mb 7800Mhz 256 bit DVI HDCP (AXRX 580 4GBD5-DMV2)',
            'ASUS Radeon RX 470 1206Mhz PCI-E 3.0 8192Mb (Samsung) 7000Mhz 256',
            'PowerColor Radeon RX 570 1105Mhz PCI-E 3.0 8192Mb 7000Mhz 256 bit DVI HDCP Red Dragon (AXRX 570 8GBD5-DMV3) White Box',
            'MSI Radeon RX 570 1244Mhz PCI-E 3.0 8192Mb 7000Mhz 256 bit DVI-D MINER OEM (RX 570 MINER 8G)',
            'MSI GeForce GTX 1050 Ti 1379Mhz PCI-E 3.0 4096Mb 7108Mhz 128 bit DVI HDMI HDCP (GTX 1050 Ti GAMING X 4G)',
            'GIGABYTE Radeon RX 570 8192Mb GAMING MI (GV-RX570GAMING-8GD-MI',
            'Sapphire Radeon PULSE RX 570 PCI-E 3.0 8192Mb (11266-66-20G)',
            'GIGABYTE GeForce GTX 1650 SUPER 4096Mb (GV-N165SWF2OC-4GD)',

            /* ram */
            'Оперативная память Patriot SO-DIMM DDR2 2Gb 800MHz pc-6400',
            'Оперативная память Patriot DDR3 4Gb 1600MHz pc-12800',
            'Оперативная память Patriot SO-DIMM DDR3 4Gb 1600MHz pc-12800',
            'Оперативная память Crucial DDR4 4Gb 2400MHz pc-19200',
            'Оперативная память Kingston DDR4 4Gb 2400MHz pc-19200',

            /* pawer adapter */
            'HIPER HPT-500 500W OEM',
            '500W Exegate 500NPX ATX EX224734RUS',
            'HIPER HPT-450 450W OEM',
            'CROWN CM-PS600W PLUS (CM000003018)',
            'COUGAR STE500 ATX 500W',

            /* ssd */
            'PATRIOT 2.5" Burst 120 Гб SATA III TLC PBU120GS25SSDR',
            'WESTERN DIGITAL 2.5" Green 120Gb SATA III 3D TLC (WDS120G2G0A)',
            'SANDISK 2.5" Plus 120 Гб SATA III 3D NAND (SDSSDA-120G-G27)',
            'TRANSCEND 2.5" SSD220 120 Гб SATA III TLC TS120GSSD220S',
            'ADATA 2.5" SU650 120 Гб SATA III TLC 3D NAND (ASU650SS-120GT-R)',

            /* hdd */
            'Western Digital Blue 2.5" 500 Gb SATA III 16 Mb 5400 rpm WD5000LPCX',
            'Toshiba P300 3.5" 1.0 Tb SATA III 64 Mb 7200 rpm HDWD110UZSVA',
            'Western Digital Caviar Blue 3.5" 1.0 Tb SATA III 64 Mb 7200 rpm WD10EZEX',
            'Seagate Barracuda 3.5" 1.0 Tb SATA III 64 Mb 7200',
            'Western Digital Blue 3.5 " 1.0 Tb SATA III 64 Mb 5400',

            /* Звуковая карта */
            'Звуковая карта Asus PCI XONAR SE',
            'Усилитель для наушников Sennheiser GSX 1200 Pro для ПК',
            'Звуковая карта ASUS Strix Raid DLX 7.1',

            /* cooler */
            'Кулер для процессора DeepCool GAMMA ARCHER',
            'Кулер для процессора Deepcool THETA 20 PWM',
            'Кулер для процессора DEEPCOOL ICE EDGE Mini FS V2.0 RET',
            'Кулер для процессора ID-Cooling SE-912i-B',
            'Кулер для процессора ID-Cooling SE-912i-R',

            /* accessory */
            'Контроллер Deepcool FH-04 для вентиляторов',
            'Хаб для вентиляторов Thermaltake Commander FP (AC-023-AN1NAN-A1)',
            'Конвертер DEEPCOOL RGB CONVERTOR 5V in 12V',
            'Комплект райзер и кронштейн для крепления VGA карты',
            'Контроллер Cooler Master A-RGB LED (MFX-ACBN-NNUNN-R1)',
        ];

        foreach($names as $name) {
            DB::table('products')->insert([
                'title' => $name,
                'slug' => $name,
                'description' => 'Некоторое описание '.$name.' как товара.',
                'brand_id' => rand(1, 20),
                'type_id' => rand(1, 10), 
                'price' => rand(5, 100) * 100
            ]);
        }
    }
}

<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            'title' => 'Материнская плата',
            'slug' => 'motherboard',
            'description' => 'Некоторое описание материнской платы как типа товара.',
        ]);

        DB::table('types')->insert([
            'title' => 'Процессор',
            'slug' => 'cpu',
            'description' => 'Некоторое описание процессора как типа товара.',
        ]);

        DB::table('types')->insert([
            'title' => 'Оперативаная память',
            'slug' => 'ram',
            'description' => 'Некоторое описание оперативной памяти как типа товара.',
        ]);

        DB::table('types')->insert([
            'title' => 'Блок питяания',
            'slug' => 'power_adapter',
            'description' => 'Некоторое описание блока питания как типа товара.',
        ]);

        DB::table('types')->insert([
            'title' => 'Видеокарта',
            'slug' => 'videocard',
            'description' => 'Некоторое описание видеокарты как типа товара.',
        ]);

        DB::table('types')->insert([
            'title' => 'Звуковая карта',
            'slug' => 'sound_card',
            'description' => 'Некоторое описание звуковой карты как типа товара.',
        ]);

        DB::table('types')->insert([
            'title' => 'Куллер',
            'slug' => 'cooler',
            'description' => 'Некоторое описание куллера как типа товара.',
        ]);

        DB::table('types')->insert([
            'title' => 'SSD диск',
            'slug' => 'ssd_disk',
            'description' => 'Некоторое описание ssd диска как типа товара.',
        ]);

        DB::table('types')->insert([
            'title' => 'HDD диск',
            'slug' => 'hdd_disk',
            'description' => 'Некоторое описание hdd диска как типа товара.',
        ]);

        DB::table('types')->insert([
            'title' => 'Аксессуары',
            'slug' => 'accessory',
            'description' => 'Некоторое описание аксессуаров как типа товара.',
        ]);
    }
}

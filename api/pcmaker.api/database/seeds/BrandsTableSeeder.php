<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'asus', 'intel', 'msi', 'amd', 'asrock',
            'gigabyte', 'hp', 'dell', 'nvidia', 'palit',
            'sapphire', 'aerocool', 'kingston', 'Western Didgital', 'seagate',
            'toshiba', 'creative', 'sennheiser', 'biostar', 'ecs'
        ];
        
        foreach($names as $name) {
            DB::table('brands')->insert([
                'title' => $name,
                'slug' => $name,
                'description' => 'Некоторое описание '.$name.' как бренда.',
            ]);
        }
    }
}

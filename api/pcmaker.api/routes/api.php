<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(
    ['prefix' => 'api', 'namespace' => 'API'], function () {
        Route::get('types', 'TypeController@index');
        Route::get('types/{type}', 'TypeController@show');
        Route::post('types', 'TypeController@store');
        Route::put('types/{type}', 'TypeController@update');
        Route::delete('types/{type}', 'TypeController@destroy');

        Route::get('brands', 'BrandController@index');
        Route::get('brands/{brand}', 'BrandController@show');
        Route::post('brands', 'BrandController@store');
        Route::put('brands/{brand}', 'BrandController@update');
        Route::delete('brands/{brand}', 'BrandController@destroy');

        Route::get('products', 'ProductController@index');
        Route::get('products/{product}', 'ProductController@show');
        Route::post('products', 'ProductController@store');
        Route::post('products/{product}', 'ProductController@update');
        Route::delete('products/{product}', 'ProductController@destroy');

        Route::get('orders', 'OrderController@index');
        Route::get('orders/{order}', 'OrderController@show');
    }
);

Route::group(['prefix' => 'api/web'],function () {
        Route::get('types', 'IndexController@types');

        Route::post('orders', 'OrderController@store');
    }
);


import axios from 'axios'

export const state = () => ({
  /* save onli product id */
  list: [],
  current: [],
  one: []
})

export const actions = {
  async fetchAll({ commit }) {
    const { data } = await axios.get('/api/orders')
    commit('saveAll', data)
    return data
  },

  async fetch({ commit }, id) {
    const { data } = await axios.get(`/api/orders/${id}`)
    commit('save', data)
    return data
  },

  /* web */
  async post({ commit }, params) {
    const { data } = await axios.post('/api/web/orders', params)
    commit('addList', data)
    return data
  },

  save({ commit }, id) {
    commit('add', id)
  },

  remove({ commit }, id) {
    commit('remove', id)
  },

  clear({ commit }) {
    commit('clear')
  }
}

export const mutations = {
  saveAll(state, data) {
    state.list = data
  },

  save(state, data) {
    state.one = data
  },

  addList(state, data) {
    state.list.push(data)
  },

  add(state, data) {
    state.current.unshift(data)
  },

  clear(state) {
    state.current = []
  },

  remove(state, id) {
    state.current.forEach((item, index, object) => {
      if (item == id) return object.splice(index, 1)
    })
  }
}

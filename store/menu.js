export const state = () => ({
  list: [
    { id: 0, title: 'комплектующие', path: '/accessories' },
    { id: 1, title: 'моя сборка', path: '/order' }
  ],
  controlList: [
    { id: 0, title: 'типы', path: '/control/types' },
    { id: 1, title: 'бренды', path: '/control/brands' },
    { id: 2, title: 'продукты', path: '/control/products' },
    { id: 3, title: 'заказы', path: '/control/orders' }
  ]
})

export const getters = {
  getList: state => {
    return state.list
  },

  getControlList: state => {
    return state.controlList
  }
}

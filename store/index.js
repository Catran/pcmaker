export const actions = {
  nuxtClientInit({ dispatch }) {
    dispatch('types/fetchAll')
    dispatch('brands/fetchAll')
    dispatch('products/fetchAll')
    dispatch('orders/fetchAll')
  }
}

import axios from 'axios'

export const state = () => ({
  list: [],
  one: {
    title: null,
    slug: null,
    description: null
  }
})

export const actions = {
  async fetchAll({ commit }) {
    const { data } = await axios.get('/api/types')
    commit('saveAll', data)
    return data
  },

  async fetch({ commit }, id) {
    const { data } = await axios.get(`/api/types/${id}`)
    commit('save', data)
    return data
  },

  async post({ commit }, params) {
    const { data } = await axios.post('/api/types', params)
    commit('add', data)
    return data
  },

  async del({ commit }, id) {
    await axios.delete(`/api/types/${id}`)
    commit('remove', id)
  }
}

export const mutations = {
  saveAll(state, data) {
    state.list = data
  },

  save(state, data) {
    state.one = data
  },

  add(state, data) {
    state.list.push(data)
  },

  remove(state, id) {
    state.list.forEach((item, index, object) => {
      if (item.id == id) return object.splice(index, 1)
    })
  }
}

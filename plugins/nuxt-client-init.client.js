export default async context => {
  if (context.route.path.includes('control')) {
    await context.store.dispatch('nuxtClientInit', context)
  }
}
